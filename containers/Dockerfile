# -*- mode: dockerfile -*-
# # vi: set ft=dockerfile :

FROM opensuse/leap:15.5

RUN zypper ref \
    && zypper -n --no-gpg-checks in --recommends -t pattern \
    devel_basis \
    devel_C_C++ \
    devel_qt6 \
    && zypper -n --no-gpg-checks in --recommends \
    libfuse2 \
    pulseaudio \
    patchelf \
    ninja \
    gcc13 \
    gcc13-c++ \
    clang \
    clang-devel \
    llvm-devel \
    libclang-cpp15 \
    lld \
    libc++-devel \
    wget \
    p7zip-full \
    sudo \
    && rm -rf /var/cache/zypp/*

ENV CC clang
ENV CXX clang++
ENV LC_ALL="en_US.UTF-8"

RUN useradd -ms /bin/bash developer

# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    echo "developer:x:${uid}:${gid}:developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

RUN chmod 777 -R /home/developer

WORKDIR /home/developer
ENV HOME /home/developer
ENV QMAKE qmake6

USER developer
CMD bash
