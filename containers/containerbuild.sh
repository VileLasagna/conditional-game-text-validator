#!/bin/bash


CONTAINER_CMD="podman"
SUDO_CMD="sudo"

CMD="${SUDO_CMD} ${CONTAINER_CMD}"


${CMD} build -f Dockerfile -t cartomancer-build .
