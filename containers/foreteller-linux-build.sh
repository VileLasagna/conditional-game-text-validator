#!/bin/bash

#Cause the script to exit if any commands fail
set -e
set -o pipefail

# Assign metadata
GIT_COMMIT=$(git rev-parse HEAD)

display_help() {
    echo "$(basename "$0") [OPTIONS] -- build Foreteller" >& 2
    echo
    echo "where:"
    echo "    -h    Show this help text"
    echo "    -c    Do a clean build from scratch"
    echo "    -b    \"Bash into\" build environment instead of actually running the build"
}

PRECMD=":"

LINUXDEPLOY_URL="https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage"
LINUXDEPLOY_QT_URL="https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage"

CMAKECMD="cmake \
            -DCMAKE_CXX_COMPILER=clang++ \
            -DCMAKE_C_COMPILER=clang \
            -DCMAKE_BUILD_TYPE=Release \
            -DBUILD_FORETELLER=ON \
            -G \"Unix Makefiles\" \
            -DCMAKE_INSTALL_PREFIX=/usr"

POSTCMD="make install DESTDIR=AppDir \
        && wget ${LINUXDEPLOY_URL} \
        && wget ${LINUXDEPLOY_QT_URL} \
        && chmod +x linuxdeploy*.AppImage \
        && QML_SOURCES_PATHS=/cartomancer/tool/qml \
                ./linuxdeploy-x86_64.AppImage \
                --appdir AppDir \
                -i /cartomancer/Cartomancer-logo.png \
                -d /cartomancer/tool/Foreteller.desktop \
                --plugin qt \
                --output appimage"


BUILD_TYPE="RelWithDebInfo"
INSTALL_DIR="/usr"
CLEAN_BUILD=0
CONTAINER_INTERACTIVE="-ti"
SQUASH_IMAGE=""
BASH_INTO=0

BASH_CMD="bash"
SUDO_CMD="sudo"
CONTAINER_CMD="podman"
BUILD_ENV="cartomancer-build"

while getopts ":hcngpbidr" opt; do
  case "$opt" in
    h)
        display_help
        exit 0
        ;;
    c)
        CLEAN_BUILD=1
        ;;
    b)
        BASH_INTO=1
        CONTAINER_CMD="podman"
        ;;
    \?)
       echo "Invalid parameter"
       echo "$0 -h for more help"
       exit 1
       ;;
  esac
done

if [ $CLEAN_BUILD -eq 1 ]; then
    rm -rf ./build || true
fi

if [ ! -d ./build ]; then
    mkdir build
fi

if [ $BASH_INTO -eq 1 ]; then

    ${SUDO_CMD} ${CONTAINER_CMD} run --rm $CONTAINER_INTERACTIVE \
    --mount type=bind,src=$(pwd)/..,target=/cartomancer \
    --device /dev/fuse \
    --cap-add SYS_ADMIN \
    -w /cartomancer/containers \
    ${BUILD_ENV} \
    $BASH_CMD

else

    ${SUDO_CMD} ${CONTAINER_CMD} run --rm $CONTAINER_INTERACTIVE \
    --mount type=bind,src=$(pwd)/..,target=/cartomancer \
    --device /dev/fuse \
    --cap-add SYS_ADMIN \
    -w /cartomancer/containers/build \
    ${BUILD_ENV} \
    $BASH_CMD -c "$(echo "$PRECMD") \
    && ${CMAKECMD} /cartomancer \
    && cmake --build . --parallel \
    && $POSTCMD "

fi
