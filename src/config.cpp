/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#include "cartomancer/config.hpp"

namespace Cartomancer {

Config::Config( std::string DelimiterStartString
              , std::string DelimiterEndString
              , std::string StatementSeparatorString
              , std::string ArgumentSeparatorString
              , std::string ChildItemPlaceholderString)
    : delimiterStart{std::move(DelimiterStartString)}
    , delimiterEnd{std::move(DelimiterEndString)}
    , statementSeparator{std::move(StatementSeparatorString)}
    , argumentSeparator{std::move(ArgumentSeparatorString)}
    , childItemPlaceholder{std::move(ChildItemPlaceholderString)}
{}

std::string Config::DelimiterStart() const noexcept
{
    return delimiterStart;
}
std::string Config::DelimiterEnd() const noexcept
{
    return delimiterEnd;
}
std::string Config::StatementSeparator() const noexcept
{
    return statementSeparator;
}
std::string Config::ArgumentSeparator() const noexcept
{
    return argumentSeparator;
}
std::string Config::ChildItemPlaceholder() const noexcept
{
    return childItemPlaceholder;
}

void Config::setDelimiterStart(const std::string& newDelimiterStart)
{
    delimiterStart = newDelimiterStart;
}

void Config::setDelimiterEnd(const std::string& newDelimiterEnd)
{
    delimiterEnd = newDelimiterEnd;
}

void Config::setStatementSeparator(const std::string& newStatementSeparator)
{
    statementSeparator = newStatementSeparator;
}

void Config::setArgumentSeparator(const std::string& newArgumentSeparator)
{
    argumentSeparator = newArgumentSeparator;
}

void Config::setChildItemPlaceholder(const std::string& newChildItemPlaceholder)
{
    childItemPlaceholder = newChildItemPlaceholder;
}

} // namespace Cartomancer
