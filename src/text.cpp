/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#include "cartomancer/text.hpp"
#include "cartomancer/condition.hpp"

#include <stdexcept>

Cartomancer::Text::Text(std::shared_ptr<Cartomancer::Context>  ctxt, std::string input)
    : fullString{input}
{
    if (ctxt == nullptr)
    {
        throw std::runtime_error("Creating Cartomancer::Text with null context!");
    }
    // Step 1 - Trim and Untangle
    // Step 2 - Trim Delimiters and Split Statements
    // Step 3 - Create/Update Condition and Create and store children
    auto items = Cartomancer::String::untangle(ctxt->Config(), input);
    for(size_t i = 1; i < items.size(); i++)
    {
        children.emplace_back(ctxt, std::move(items[i]));
    }

    auto statements = Cartomancer::String::splitStatements(ctxt->Config(), items[0]);

    // Create our condition and store the ranges, if any.
    // Then send it to context so it can be part of the set

    Cartomancer::Condition newCondition{ctxt, statements};

    for(const auto& r : newCondition.Ranges())
    {
        ranges.push_back(r);
    }

    auto condition = ctxt->registerCondition(std::move(newCondition));
    conditionName = condition.VarName();
    textContent = std::move(statements.second);
    if(textContent.empty())
    {
        textContent.emplace_back();
    }
    if(condition.Type() == Cartomancer::Type::Range)
    {
        if(ranges.size() == 1 && textContent.size() < 2)
        {
            // A range with a single threshold means "this text if variable is larger than VALUE, no text otherwise"
            textContent.emplace_back("");
        }
        else
        {
            if(textContent.size() < ranges.size())
            {
                // [myVar 10 21| someText] means "someText" will show if myVar is between [10,21), but nothing otherwise

                textContent.emplace_back("");
            }
            // not ideal for a vector but I'm opting for it anyway just so the code is structured in a friendlier way
            // Alternatively, this vector COULD be made into a list, but I think that's worse overall
            textContent.insert(textContent.begin(), "");
        }
    }
    if(condition.Type() == Cartomancer::Type::Switch && textContent.size() < 2)
    {
        // A switch with a single item means "this text if variable is true, no text otherwise"
        textContent.emplace_back("");
    }
}

bool Cartomancer::Text::isValid(std::shared_ptr<Cartomancer::Context>  ctxt) const
{
    if (ctxt == nullptr)
    {
        throw std::runtime_error("Cannot validate Cartomancer::Text against null context!");
    }
    size_t num_placeholders = 0;
    size_t last_match = textContent[0].find(ctxt->Config().ChildItemPlaceholder(), 0);
    while (last_match != std::string::npos)
    {
        num_placeholders++;
        last_match = textContent[0].find(ctxt->Config().ChildItemPlaceholder(), 0);
    }

    if(num_placeholders != children.size())
    {
        return false;
    }

    // End of self-checking, iterating over children

    if(!children.empty())
    {
        for(auto& child : children)
        {
            if(!child.isValid(ctxt))
            {
                return false;
            }
        }
    }
    return true;
}

Cartomancer::Type Cartomancer::Text::Type(std::shared_ptr<Context> ctxt) const
{
    if (ctxt == nullptr)
    {
        throw std::runtime_error("Cannot read Cartomancer::Condition from null context!");
    }
    if (conditionName.empty() )
    {
        return Cartomancer::Type::Unconditional;
    }
    return ctxt->Condition(conditionName).Type();
}

const std::string& Cartomancer::Text::Content(std::shared_ptr<Context> ctxt) const
{
    if (ctxt == nullptr)
    {
        throw std::runtime_error("Cannot parse Cartomancer::Text with null context!");
    }
    auto cond = ctxt->Condition(conditionName);
    if(cond.Type() == Cartomancer::Type::Unconditional)
    {
        return textContent[0];
    }
    if(cond.Type() == Cartomancer::Type::Substitution)
    {
        try
        {
            /* "Lesser of two evils" type situation here.
             * We don't want to copy the whole string every time but we can't
             * return a reference to an external context variable. So if it IS
             * a substitution string, we store it's current value internally and
             * provide that instead
             *
             * It IS a bit of a roundabout way and, perhaps wastes a bit of memory
             * but the expectation is that these are normally going to be small
             * strings
             */
            substitutionContent = std::get<std::string>(cond.Value());
            return substitutionContent;
        }
        catch(std::bad_variant_access ex)
        {
            std::string err("Error accessing substitution string: ");
            err.append(ex.what());
            throw std::runtime_error(err);
        }
    }
    return textContent[contentIndex(ctxt)];
}

const Cartomancer::Text& Cartomancer::Text::getChild(size_t idx) const
{
    return children[idx];
}

size_t Cartomancer::Text::contentIndex(std::shared_ptr<Context> ctxt) const
{
    if (ctxt == nullptr)
    {
        throw std::runtime_error("Cannot read index in Cartomancer::Text from null context!");
    }
    auto cond = ctxt->Condition(conditionName);
    auto val = cond.Value();
    if(cond.Type() == Cartomancer::Type::Switch)
    {
        switch( val.index() )
        {
            //null
            case 0:
                // Unitialised values not considered an error
                break;
            //bool
            case 1:
                if(std::get<bool>(val))
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            //int
            case 2:
                [[fallthrough]];
            //float
            case 3:
                [[fallthrough]];
            //string
            case 4:
                for(size_t i = 0; i < ranges.size(); i++)
                {
                    if(ranges[i] == val)
                    {
                        return i;
                    }

                }
                throw std::runtime_error("Couldn't find matching range for switch condition ->" + cond.VarName());
                return 0;
                break;
        }
    }

    if(cond.Type() == Cartomancer::Type::Range)
    {
        if(val.index() != 3)
        {
            throw std::runtime_error("Range condition has non-float range thresholds -> " + cond.VarName());
        }
        for(size_t i = 0; i < ranges.size(); i++)
        {
            if(ranges[i] > val)
            {
                return i;
            }
        }
        return ranges.size();
    }

    return 0;
}
