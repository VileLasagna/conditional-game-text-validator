/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#include "cartomancer/util.hpp"
#include "cartomancer/config.hpp"

#include <map>
#include <regex>

std::pair<std::string, std::vector<std::string>> Cartomancer::String::splitStatements(const Cartomancer::Config& config, const std::string& input)
{
    //This function assumes input has already been untangled

    std::pair<std::string, std::vector<std::string>> statements;
    std::string trimmed{trimmedCopy(input)};
    if(!indexMatches(trimmed, 0, config.DelimiterStart()))
    {
        //Plain string, no condition
        statements.second.push_back(input);
        return statements;
    }

    // Since we know input must have been already untangled, we don't risk
    // getting into a malformed string from doing this to something like:
    // [~~~~~~] ~~~~~ [ ~~~~~~ |~~~~]
    // And we want to just store the contents from now on. Both delimiters
    // and statementSeparators should be gone from the return of this function
    trimDelimiters(config, trimmed);

    std::vector<size_t> separatorIndexes;
    for(size_t idx = 0; idx < trimmed.size(); idx++)
    {
        if( indexMatches(trimmed, idx, config.StatementSeparator()))
        {
            //We only want to figure out where the condition for the outermost string is
            separatorIndexes.push_back(idx);
        }
    }

    if(separatorIndexes.empty())
    {
        //This is a plain substitution string condition
        statements.first = trimmed;
        trim(statements.first);
        return statements;
    }

    size_t nextStart = 0;
    for(auto i : separatorIndexes)
    {
        if(nextStart == 0)
        {
            statements.first = trimmed.substr(0, i);
            nextStart = i + config.StatementSeparator().size();
        }
        else
        {
            statements.second.push_back(trimmed.substr(nextStart, i - nextStart));
            nextStart = i + config.StatementSeparator().size();
        }
    }
    //Don't forget the last one
    statements.second.push_back(trimmed.substr(nextStart));

    return statements;
}

std::string Cartomancer::String::decodeVar(const Cartomancer::Config& config, const std::string& input)
{
    return tokenise(config, input).front();
}

std::set<Cartomancer::condVal_t> Cartomancer::String::decodeRanges(const Cartomancer::Config& config, const std::string& input)
{
    auto tokens = tokenise(config, input);
    std::set<condVal_t> ranges;
    bool rangesAreNumbers = false;
    // If there's only one token, just return empty. Otherwise...
    if(tokens.size() > 2)
    {
        //https://stackoverflow.com/a/34471022
        rangesAreNumbers = std::regex_match( tokens[1], std::regex( ( "((\\+|-)?[[:digit:]]+)(\\.(([[:digit:]]+)?))?" ) ) );
        for(size_t i = 1; i < tokens.size(); i++)
        {
            if(rangesAreNumbers)
            {
                try
                {
                    ranges.emplace(std::stof(tokens[i]));
                }
                catch(std::invalid_argument ex)
                {
                    throw std::runtime_error("Error converting range to a numeric value -> " + tokens[i]);
                }
            }
            else
            {
                ranges.emplace(tokens[i]);
            }
        }
    }
    return ranges;
}

std::vector<std::string> Cartomancer::String::tokenise(const Config& config, const std::string& input)
{
    std::vector<std::string> tokens;

    size_t argumentStart = 0;

    for( size_t idx = 0; idx < input.length(); idx ++)
    {
        if( indexMatches(input, idx, config.StatementSeparator()) )
        {
            //TODO: warning
            tokens.emplace_back(input.substr(argumentStart, idx - argumentStart));
            break;
        }
        if( indexMatches(input, idx, config.ArgumentSeparator()) )
        {
            auto t = trimmedCopy(input.substr(argumentStart, idx - argumentStart));
            if(! t.empty())
            {
                tokens.emplace_back(std::move(t));
            }
            argumentStart = idx+config.ArgumentSeparator().size();
        }
    }
    tokens.emplace_back(input.substr(argumentStart, input.size() - argumentStart));
    for(auto& s: tokens)
    {
        for(size_t i = 0; i < s.size(); i++)
        {
            if(isspace(s[i]))
            {
                s.erase(i);
            }
        }
    }
    return tokens;
}

void Cartomancer::String::trimDelimiters(const Config& config, std::string& input)
{
    if(input.size() < 2)
    {
        //TODO: Warning
        return;
    }
    size_t idx = 0;
    while(isspace(input[idx]))
    {
        idx++;
        if(idx == input.size())
        {
            //All blanks, somehow?
            return;
        }
    }
    if(indexMatches(input, idx, config.DelimiterStart()))
    {
        input.erase(idx, config.DelimiterStart().size());
    }
    idx = input.size() - 1;
    while(isspace(input[idx]))
    {
        if(idx == 0)
        {
            //All blanks, somehow?
            return;
        }
        idx--;
    }
    if(indexMatches(input, idx, config.DelimiterEnd()))
    {
        input.erase(idx, config.DelimiterEnd().size());
    }
}

std::vector<std::string> Cartomancer::String::untangle(const Cartomancer::Config& config, const std::string& input)
{
    std::vector<std::string> strings{input};
    Cartomancer::String::trim(strings.front());

    bool maybeMoreChildren = true;

    while(maybeMoreChildren)
    {
        //Let's find everywhere we're opening or closing a delimiter
        std::map<size_t, bool> delimiters;
        size_t lastIdx = 0;
        do
        {
            lastIdx = strings.front().find(config.DelimiterStart(), lastIdx);
            if(lastIdx != std::string::npos)
            {
                delimiters.insert({lastIdx, true});
                lastIdx += config.DelimiterStart().size();
            }
        }
        while(lastIdx != std::string::npos);

        lastIdx = 0;
        do
        {
            lastIdx = strings.front().find(config.DelimiterEnd(), lastIdx);
            if(lastIdx != std::string::npos)
            {
                delimiters.insert({lastIdx, false});
                lastIdx += config.DelimiterEnd().size();
            }
        }
        while(lastIdx != std::string::npos);

        if (delimiters.size() == 1)
        {
            throw std::runtime_error("Malformed string, only a single delimiter -> " + input);
        }

        // If we start the whole string with a DelimiterStart and end it
        // with a DelimiterEnd we need to check if our outermost layer is
        // a conditional string itself, lest it becomes a child of itself
        if(   (delimiters.size() >= 2)
          &&  (delimiters.begin()->first == 0)
          &&  (delimiters.begin()->second)
          &&  (delimiters.rbegin()->first == (strings.front().size() - 1) )
          && !(delimiters.rbegin()->second) )
        {
            //TODO : Maybe a good opportunity to check for malformed?
            bool fullCondition = false;
            size_t openCount = 0;
            for(auto& item: delimiters)
            {
                if(item.second)
                {
                    openCount++;
                }
                else
                {
                    if(openCount == 0)
                    {
                        throw std::runtime_error("Malformed string, condition closed without having been open -> " + input);
                    }
                    openCount--;
                    if(openCount == 0)
                    {
                        if( item == *delimiters.rbegin())
                        {
                            fullCondition = true;
                        }
                        break;
                    }
                }
            }
            if (openCount > 0)
            {
                throw std::runtime_error("Malformed string, condition not closed in -> " + input);
            }
            if(fullCondition)
            {
                delimiters.erase(delimiters.begin());
                delimiters.erase(delimiters.rbegin()->first);
            }
        }

        if(delimiters.empty())
        {
            maybeMoreChildren = false;
        }
        else
        {
            //Do some counting to figure out where we open and close the
            // "outermost" condition
            size_t openCount = 0;
            size_t openIndex = 0;
            size_t closeIndex = 0;
            for(auto& item: delimiters)
            {
                if(item.second)
                {
                    openCount++;
                    if(openCount == 1)
                    {
                        openIndex = item.first;
                    }
                }
                else
                {
                    if(openCount == 0)
                    {
                        throw std::runtime_error("Malformed string, condition closed without having been open -> " + input);
                    }
                    openCount--;
                    if(openCount == 0)
                    {
                        closeIndex = item.first;
                        break;
                    }
                }
            }
            if( (openIndex + config.DelimiterStart().size()) >= closeIndex )
            {
                throw std::runtime_error("Error untangling string, cannot close condition in -> " + input);
            }
            auto placeholderTag = config.ChildItemPlaceholder() + char(strings.size() - 1);
            //Copy out the child
            strings.emplace_back(strings[0].substr(openIndex, closeIndex-openIndex + config.DelimiterEnd().size()));
            //Replace it with a tag
            strings[0].replace(openIndex, closeIndex-openIndex + config.DelimiterEnd().size(), placeholderTag);
        }
    } //while(maybeMoreChildren)

    return strings;
}


/*
 * Shoutout to:
 * https://stackoverflow.com/questions/216823/how-to-trim-an-stdstring/217605#217605
 */

// trim from start (in place)
void Cartomancer::String::ltrim(std::string &s)
{
    s.erase( s.begin()
            , std::find_if( s.begin()
                         , s.end()
                         , [](char ch)
                         {
                             return !isspace(ch);
                         } )  );
}

// trim from end (in place)
void Cartomancer::String::rtrim(std::string &s)
{
    s.erase( std::find_if( s.rbegin()
                         , s.rend()
                         , [](char ch)
                         {
                             return !isspace(ch);
                         }
                         ).base()
            , s.end());
}

// trim from both ends (in place)
void Cartomancer::String::trim(std::string& s) {
    rtrim(s);
    ltrim(s);
}

std::string Cartomancer::String::trimmedCopy(const std::string& s) {
    auto ret = s;
    rtrim(ret);
    ltrim(ret);
    return ret;
}

bool Cartomancer::String::indexMatches(const std::string& input, size_t index, const std::string& tokenToMatch)
{
    return(  (index + tokenToMatch.size() <= input.size())
            && input.substr(index,tokenToMatch.size()) == tokenToMatch );
}
