/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#include "cartomancer/condition.hpp"
#include <stdexcept>
#include <utility>
#include <vector>

Cartomancer::Condition::Condition(std::string name)
    : condVar{std::move(name)}
    , type{Type::INVALID}
{}

void Cartomancer::Condition::updateRanges(const Condition& other)
{
    if(*this != other)
    {
        throw std::runtime_error("Attempting to update condition with different condition");
    }
    for(const auto& r: other.Ranges())
    {
        ranges.insert(r);
    }
}

Cartomancer::Condition::Condition(std::string&& variableName, std::set<condVal_t>&& rangeThresholds, Cartomancer::Type t)
    : condVar{variableName}
    , ranges{rangeThresholds}
    , type{t}
{}

Cartomancer::Condition::Condition(std::shared_ptr<Context>& ctxt, const std::string& decoratedStringInput)
    :context{ctxt}
{
    /*
     * Three cases here:
     * 1 -> If the first character is NOT "delimiterStart" then this is unconditional
     * 2 -> If there is no "statementSeparator" between "delimiterStart" and "delimiterEnd" this is a substitution
     * 3 -> We're dealing with either a "switch" or "range" and will need to check for argument count to find out
     */
    auto trimmed{Cartomancer::String::trimmedCopy(decoratedStringInput)};
    // Split up the string
    if(trimmed.length() < 1)
    {
        return;
    }

    if( Cartomancer::String::indexMatches(trimmed, 0, ctxt->Config().DelimiterStart()) )
    {
        type = Type::Unconditional;
        return;
    }

    decodeCondition(trimmed);
}

Cartomancer::Condition::Condition(std::shared_ptr<Context>& ctxt, const decomposedStr& items)
    :context{ctxt}
{
    if(items.first.empty())
    {
        type = Type::Unconditional;
        return;
    }
    condVar = Cartomancer::String::decodeVar(ctxt->Config(), items.first);
    if(items.second.empty())
    {
        type = Type::Substitution;
        value = condVar;
        return;
    }
    ranges = Cartomancer::String::decodeRanges(ctxt->Config(), items.first);
    if(ranges.empty())
    {
        type = Type::Switch;
        if(items.second.size() <= 2)
        {
            ranges.insert(true);
            ranges.insert(false);
        }
        else
        {
            for(size_t i = 0; i < items.second.size(); i++)
            {
                ranges.insert(static_cast<int>(i));
            }
        }
    }
    else
    {
        if(ranges.begin()->index() == 4)
        {
            //This is a bunch fo strings
            type = Type::Switch;
        }
        else
        {
            type = Type::Range;
        }
    }
    if(type == Type::Range)
    {
        value = 0.0f;
    }
    else
    {
        value = *ranges.begin();
    }
}

void Cartomancer::Condition::setValue(condVal_t newValue)
{
    value = std::move(newValue);
}

bool Cartomancer::Condition::operator==(const Condition& other) const
{
    if(type != Type::INVALID && other.type != Type::INVALID)
    {
        if(type != other.Type())
        {
            return false;
        }
        if( (type == Type::Unconditional) && (other.Type() == Type::Unconditional))
        {
            return true;
        }
    }
    return (VarName() == other.VarName());
}

bool Cartomancer::Condition::operator!=(const Condition& other) const
{
    return !(*this == other);
}

Cartomancer::Condition& Cartomancer::Condition::None()
{
    static Condition none{""};
    return none;
}

void Cartomancer::Condition::update(const std::string& input)
{
    auto ctxt = context.lock();
    auto newVar = Cartomancer::String::decodeVar(ctxt->Config(), input);
    auto newRanges = Cartomancer::String::decodeRanges(ctxt->Config(), input);

    if(newVar != condVar)
    {
         throw std::runtime_error("Attempting to update ranges with different conditions");
    }
    if(!newRanges.empty())
    {
        if(!ranges.empty() && (ranges.begin()->index() != newRanges.begin()->index()))
        {
           throw std::runtime_error("Attempting to update condition with ranges of a different type");
        }
        for(auto& range : newRanges)
        {
            ranges.insert(range);
        }
    }
}

void Cartomancer::Condition::decodeCondition(const std::string& s)
{
    auto ctxt = context.lock();
    condVar = Cartomancer::String::decodeVar(ctxt->Config(), s);
    ranges = Cartomancer::String::decodeRanges(ctxt->Config(), s);
}





