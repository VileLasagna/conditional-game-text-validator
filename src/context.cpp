/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#include "cartomancer/context.hpp"
#include "cartomancer/condition.hpp"


Cartomancer::Context::Context(Cartomancer::Config conf)
    : config{std::move(conf)}
{}

const Cartomancer::Config& Cartomancer::Context::Config() const noexcept
{
    return config;
}

Cartomancer::Condition& Cartomancer::Context::Condition(std::string conditionName)
{
    auto cond = conditions.find(conditionName);
    if(cond != conditions.end())
    {
        return cond->second;
    }

    return Condition::None();
}

Cartomancer::conditionMap& Cartomancer::Context::Conditions() noexcept
{
    return conditions;
}

Cartomancer::Condition& Cartomancer::Context::registerCondition(Cartomancer::Condition&& input)
{
    for (auto& p : conditions)
    {
        if(p.second == input)
        {
            p.second.updateRanges(input);
            return p.second;
        }
    }

    //new condition
    auto ret = conditions.emplace(input.VarName(), input);
    return ret.first->second;

}

std::string Cartomancer::Version() noexcept
{
    std::string ret = std::to_string(Cartomancer::versionMajor);
    ret.append(".");
    ret.append(std::to_string(Cartomancer::versionMinor));
    return ret;
}

