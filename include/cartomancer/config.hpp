/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#ifndef CARTOMANCER_CONFIG_HPP_DEFINED
#define CARTOMANCER_CONFIG_HPP_DEFINED

#include <string>

namespace Cartomancer {

class Config
{
public:
    Config( std::string DelimiterStartString = "["
          , std::string DelimiterEndString = "]"
          , std::string StatementSeparatorString = "|"
          , std::string ArgumentSeparatorString = " "
          , std::string ChildItemPlaceholderString = u8"\xEF\xBF\xBC");

    [[nodiscard]] std::string DelimiterStart()       const noexcept;
    [[nodiscard]] std::string DelimiterEnd()         const noexcept;
    [[nodiscard]] std::string StatementSeparator()   const noexcept;
    [[nodiscard]] std::string ArgumentSeparator()    const noexcept;
    [[nodiscard]] std::string ChildItemPlaceholder() const noexcept;

    void setDelimiterStart(const std::string& newDelimiterStart);
    void setDelimiterEnd(const std::string& newDelimiterEnd);
    void setStatementSeparator(const std::string& newStatementSeparator);
    void setArgumentSeparator(const std::string& newArgumentSeparator);
    void setChildItemPlaceholder(const std::string& newChildItemPlaceholder);

private:

    std::string delimiterStart;
    std::string delimiterEnd;
    std::string statementSeparator;
    std::string argumentSeparator;
    std::string childItemPlaceholder;
};

} // namespace Cartomancer

#endif // CARTOMANCER_CONFIG_HPP_DEFINED
