/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#ifndef CARTOMANCER_CONTEXT_HPP
#define CARTOMANCER_CONTEXT_HPP

#include "cartomancer/util.hpp"
#include "cartomancer/config.hpp"


namespace Cartomancer {

    #ifndef CARTOMANCER_VERSION_MAJOR
        constexpr const size_t versionMajor = 0;
    #else
        constexpr const size_t versionMajor = CARTOMANCER_VERSION_MAJOR;
    #endif

    #ifndef CARTOMANCER_VERSION_MINOR
        constexpr const size_t versionMinor = 0;
    #else
        constexpr const size_t versionMinor = CARTOMANCER_VERSION_MINOR;
    #endif

    std::string Version() noexcept;

    /*
     * This class represents the overarching data multiple Text items may need to rely on
     * The Config defines the various string markers for separating items and conditions, etc
     * Changing it would potentially invalidate all currently existing Text items which are
     * based on this context and, as such, it is not allowed
     *
     * The Context also holds the collection of all Conditions registered by created Text items
     * Values must be set and read from the conditions in this collection so as to avoid duplicates
     */
class Context
{
public:
    Context(Cartomancer::Config config = {});

    [[nodiscard]] const Cartomancer::Config &Config() const noexcept;
    [[nodiscard]] Cartomancer::Condition& Condition(std::string conditionName);
    [[nodiscard]] conditionMap& Conditions() noexcept;

    [[nodiscard]] Cartomancer::Condition& registerCondition(Cartomancer::Condition&& input);

private:
    Cartomancer::Config config;
    Cartomancer::conditionMap conditions;
};

} // namespace Cartomancer

#endif // CARTOMANCER_CONTEXT_HPP
