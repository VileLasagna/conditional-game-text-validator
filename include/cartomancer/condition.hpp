/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#ifndef CARTOMANCER_CONDITION_HPP_DEFINED
#define CARTOMANCER_CONDITION_HPP_DEFINED

#include "cartomancer/util.hpp"
#include "cartomancer/context.hpp"

namespace Cartomancer
{

class Condition
{
public:

    Condition(std::string name = "");
    Condition(std::shared_ptr<Cartomancer::Context>& ctxt, const std::string& fullConditionString);
    Condition(std::shared_ptr<Cartomancer::Context>& ctxt, const Cartomancer::decomposedStr& items);
    Condition(std::string&& variableName, std::set<condVal_t>&& rangeThresholds, Cartomancer::Type t);

    Condition(const Condition& other) = default;
    Condition(Condition&& other) noexcept = default;

    Condition& operator=(const Condition& other) = default;
    Condition& operator=(Condition&& other) noexcept = default;

    void updateRanges(const Cartomancer::Condition& other);

    ~Condition() = default;

    void setValue(condVal_t newValue);
    [[nodiscard]] const std::string& VarName() const noexcept {return condVar;}
    [[nodiscard]] const condVal_t& Value() const noexcept {return value;}
    [[nodiscard]] const std::set<condVal_t>& Ranges() const noexcept {return ranges;}
    [[nodiscard]] Cartomancer::Type Type() const noexcept {return type;}


    size_t getOptionIndex();

    bool operator==(const Condition& other) const;
    bool operator!=(const Condition& other) const;

    static Condition& None();

private:

    void update(const std::string& input);

    void decodeCondition(const std::string& s);

    std::weak_ptr<Cartomancer::Context> context;
    std::string condVar;
    std::set<condVal_t> ranges;
    Cartomancer::Type type;
    condVal_t value;
};

} //namespace Cartomancer

#endif // CARTOMANCER_CONDITION_HPP_DEFINED
