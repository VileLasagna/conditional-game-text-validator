/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#ifndef CARTOMANCER_UTIL_HPP_DEFINED
#define CARTOMANCER_UTIL_HPP_DEFINED

#include <memory>
#include <map>
#include <set>
#include <string>
#include <variant>
#include <vector>

namespace Cartomancer
{

/*
    * This is how these formats are supposed to be interpreted
    * Boolean type conditionals are considered a special case
    * of the Switch conditional
    *
    * SOME_TEXT -> Unconditional (plain text)
    * [CONDITION] -> Substitution (i.e: pc.Name, char.Class...)
    * [CONDITION | True_Text ]                   -> Switch
    * [CONDITION | True_Text | False_Text ]      -> Switch
    * [CONDITION | A_Text | B_Text | C_Text ...] -> Switch
    * [CONDITION VAL | Text ]                    -> Range
    * [CONDITION VAL VAL ...| Text | Text ...]   -> Range
    */
enum class Type {
    INVALID,
    Unconditional,
    Substitution,
    Switch,
    Range
};

class Condition;
class Config;
using condVal_t    = std::variant<std::monostate, bool, int, float, std::string>;
using conditionPtr = std::shared_ptr<Condition>;
using conditionMap = std::map<std::string, Condition>;
using decomposedStr = std::pair<std::string, std::vector<std::string>>;

/*
 * This inner namespace is intended for string manipulation functions
 * that the cartomancer classes need to interpret and store data
 */
namespace String
{

    [[nodiscard]] decomposedStr splitStatements(const Cartomancer::Config& config, const std::string& input);
    [[nodiscard]] std::string decodeVar(const Cartomancer::Config& config, const std::string& input);
    [[nodiscard]] std::set<condVal_t> decodeRanges(const Cartomancer::Config& config, const std::string& input);
    [[nodiscard]] std::vector<std::string> tokenise(const Cartomancer::Config& config, const std::string& input);
    [[nodiscard]] std::vector<std::string> untangle(const Cartomancer::Config& config, const std::string& input);
    [[nodiscard]] bool indexMatches(const std::string& input, size_t index, const std::string& tokenToMatch);

    void trimDelimiters(const Config& config, std::string& s);

    void ltrim(std::string &s);
    void rtrim(std::string &s);
    void trim(std::string &s);

    std::string trimmedCopy(const std::string& s);
}

}

#endif //CARTOMANCER_UTIL_HPP_DEFINED
