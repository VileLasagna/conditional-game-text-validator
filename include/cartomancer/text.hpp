/** Cartomancer - A conditional text parsing library
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 *
 * This is anti-capitalist software, released for free use by individuals
 * and organizations that do not operate by capitalist principles: you can
 * redistribute it and/or modify it under the terms of the
 * ANTI-CAPITALIST SOFTWARE LICENSE (v 1.4-ml)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * ANTI-CAPITALIST SOFTWARE LICENSE for more details.
 *
 * You should have received a copy of the ANTI-CAPITALIST SOFTWARE LICENSE
 * along with this program.
 * If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/LICENSE.MD>.
 */

#ifndef CARTOMANCER_TEXT_HPP_DEFINED
#define CARTOMANCER_TEXT_HPP_DEFINED

#include <string>
#include <vector>

#include "cartomancer/util.hpp"
#include "cartomancer/context.hpp"

namespace Cartomancer
{

class Text
{

public:
    Text(std::shared_ptr<Cartomancer::Context>  ctxt, std::string input);

    [[nodiscard]] bool isValid(std::shared_ptr<Context> ctxt) const;

    [[nodiscard]] Cartomancer::Type Type(std::shared_ptr<Context> ctxt) const;
    [[nodiscard]] std::string ConditionName() const noexcept {return conditionName;}
    [[nodiscard]] const std::vector<condVal_t>& ConditionRanges() const noexcept {return ranges;}

    [[nodiscard]] const std::string& Content(std::shared_ptr<Context> ctxt) const;
    [[nodiscard]] const std::vector<Text>& Children() const noexcept { return children;}
    [[nodiscard]] const Text& getChild(size_t idx) const;

private:

    size_t contentIndex(std::shared_ptr<Context> ctxt) const;

    std::string conditionName;
    std::vector<condVal_t> ranges;
    std::vector<Text> children;
    std::vector<std::string> textContent;
    std::string fullString;
    mutable std::string substitutionContent;

};

} //namespace Cartomancer

#endif // CARTOMANCER_TEXT_HPP_DEFINED
