It had been a long time since [pc.Name] had visited the temple. Normally [pc.Pronouns|he|she|they] would
try [pc.Pronouns|his|her|their] best to avoid the place but desperate times call for desperate measures.

[church.Allied
|It was a delicate situation, [pc.Name] was sure the Bishop had neither forgiven nor
forgot what happened at Cartia. It was a great risk to approach her but perhaps that very
fact would clue her in as to the gravity of the situation to begin with. [npc2.Rescued|[pc.Name] didn't
really expect much gratitude when saving the paladin Cassius from a situation he himself had
created, though the paladin did honour his bonds and insisted for the Bishop to at least listen.
It was as close as it would get to actually having any allies here, and [pc.Name] wasn't about to let
this one small blessing slip by. |That the Bishop didn't order [pc.Name] killed on sight
was about as much luck as fate seemed to have in store for [pc.Pronouns|him|her|them]. It very
nearly came to violence and the Bishop only finally agreed on hearing what [pc.Name] had to say
after demanding a hefty "donation"] // npc2.rescued END
|Having sided with the Church at Cartia certainly had its perks. Though she insisted she was
terribly busy, the Bishop promptly welcomed [pc.Name] with open arms. [pc.Pronouns|he|she|they]
sat down on the luxurious guest couch on the Bishop's office, waiting for the Bishop herself while enjoying
the hospitality of the Church. The delicate tea and sweet pastries offered were a welcome respite, the
calm before the storm that was sure to rise once the Bishop heard the news.]

There was no way around the reality: things were dire. While the Church and the crown were busy at
each other's throats, the seal on the City of the Dead had been broken, though no one knew by
whom. [deathArmy.strength 2  4  6  8|So far the damage had been very localised but it was a matter
of time before the dead started marching in earnest.|The initial wave caught everyone by surprise.
What seemed to have been small scouting parties ended up revelling in wanton slaughter and already
a couple of villages had been all but destroyed.|The dead hit hard when no one really expected. They
were bold and had the power to back it up. Hundreds were either killed and forced to flee across the
realm. Even small groups of military had been routed.|The dead came out pouring from their city
seemingly in the thousands. No one was prepared for this, cities were purged, armies were ambushed and
many fell, only to be taken away. Surely these victims too would themselves be back and marching in
no time.]

What no one could figure out was who had undone the seal, let alone how or why.[pc.DarkMagic 0 70|| Though
some things did come to [pc.Name]'s mind when it came to the how. It was as good a place as any to start
investigating, but the Church wasn't likely to be happy about the direction that would go.] What was crucial
now was to get both sides of the war to agree to a truce. Every day this threat was left unchecked could
be a day too late to ever do something about it
