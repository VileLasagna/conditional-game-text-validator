/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

#include "decoratedstringprovider.hpp"
#include <QString>
#include <QHash>
#include <QFile>

#include <iostream>


#include "cartomancer/condition.hpp"

bool operator<( const StringConditionWrapper& a, const StringConditionWrapper& b)
{
    return a.getName() < b.getName();
}

bool operator==( const StringConditionWrapper& a, const StringConditionWrapper& b)
{
    return (  (a.getName()   == b.getName()  )
           && (a.getType()   == b.getType()  )
           && (a.getRanges() == b.getRanges()) );
}

size_t qHash( const StringConditionWrapper& c, size_t seed)
{
    return qHash(c.getName(), seed);
}

// All these colours are from here
// https://www.w3.org/TR/SVG11/types.html#ColorKeywords
DecoratedStringProvider::DecoratedStringProvider(QObject *parent)
    : QObject{parent}
    , default_colour{"white"}
    , subs_colours{ "springgreen"
                  , "palegreen"
                  , "limegreen"
                  , "seagreen"
                  , "olivedrab"}
    , switch_colours{ "violet"
                    , "lightcoral"
                    , "mediumvioletred"
                    , "deeppink"
                    , "purple"}
    , range_colours{ "orangered"
                   , "gold"
                   , "sandybrown"
                   , "darkorange"
                   , "wheat" }
{}

void DecoratedStringProvider::setContent(Cartomancer::Text&& s)
{
    processedContent = std::make_unique<Cartomancer::Text>(s);

    wrapConditions();
    emit contentChanged();
    errorMessage.clear();
    emit errorMessageChanged();
}

void DecoratedStringProvider::setRawContent(const QString& newContent)
{
    // I moved comment checking to here but still wondering if support
    // for comments should be a feature of Cartomancer itself, rather
    // than application logic
    rawContent = newContent;
    richFormatContent.clear();
    auto lines  = newContent.split(QChar::LineFeed);
    for(auto& s: lines)
    {
        if(s.isEmpty())
        {
            richFormatContent.append("<br><br>");
        }
        else
        {
            auto commentIdx = s.indexOf("//");
            if (commentIdx > 0)
            {
                s.remove(commentIdx, s.length());
                s.squeeze();
            }
            richFormatContent.append(s);
            richFormatContent.append(" ");
        }
    }
    try
    {
        setContent(Cartomancer::Text(context, richFormatContent.toStdString()));
    }
    catch(std::runtime_error& ex)
    {
        errorMessage = QString::fromUtf8(ex.what());
        emit errorMessageChanged();
    }
}

void DecoratedStringProvider::setContext(std::shared_ptr<Cartomancer::Context> ctxt)
{
    if(context == nullptr)
    {
        context = ctxt;
    }
    else
    {
        errorMessage = "Attempting to replace Cartomancer Content unexpectedly";
        emit errorMessageChanged();
    }
}

QString DecoratedStringProvider::CartomancerVersion() const
{
    return QString::fromStdString(Cartomancer::Version());
}

QString DecoratedStringProvider::Content() const
{
    if(processedContent == nullptr)
    {
        return "";
    }
    try
    {
        return unfold(*processedContent);
    }
    catch(std::runtime_error& ex)
    {
        errorMessage = QString::fromUtf8(ex.what());
        emit errorMessageChanged();
        return "";
    }
}

const QString& DecoratedStringProvider::RawContent() const noexcept
{
    return rawContent;
}

const QString DecoratedStringProvider::HelpText() const noexcept
{
    return loadFile(":/foretellerhelp-en.md");
}

const QString& DecoratedStringProvider::ErrorMessage() const noexcept
{
    return errorMessage;
}

void DecoratedStringProvider::reset()
{
    context = std::make_shared<Cartomancer::Context>();
    processedContent = nullptr;
    rawContent.clear();
    richFormatContent.clear();
    conditions.clear();
    errorMessage.clear();
    emit contentChanged();
    emit conditionListChanged();
    emit errorMessageChanged();
}

void DecoratedStringProvider::init()
{
    if(context == nullptr)
    {
        context = std::make_shared<Cartomancer::Context>();
    }
}

QString DecoratedStringProvider::loadFile(const QString& filepath) const
{
    QString fileContent;
    QFile inputFile(filepath);
    if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        errorMessage = "Error attempting to open file -> " + filepath;
        emit errorMessageChanged();
        return "";
    }
    fileContent = inputFile.readAll();

    return std::move(fileContent);
}

QString DecoratedStringProvider::loadFile(const QUrl& filepath) const
{
    return loadFile(filepath.toLocalFile());
}

QString DecoratedStringProvider::loadExample(size_t exampleIndex)
{
    switch(exampleIndex)
    {
    case 0:
        reset();
        return loadFile(":/teststringfull.txt");

    default:
        errorMessage = "Error attempting to load example";
        emit errorMessageChanged();
        return "";
    }
}

void DecoratedStringProvider::onConditionValueUpdated()
{
    emit contentChanged();
}

void DecoratedStringProvider::registerCondition(const Cartomancer::Text& s)
{
    /* some type conversion
     *
     * Using QVariants here is a lot more work that I'm frontloading and
     * bringing to C++ to try and avoid having unexpected behaviour in QML
     *
     * May not even be necessary, but what I want is for things like
     * assigning values and comparing in the QML side to be as smooth and
     * easy as possible
     */

    auto conditionName = s.ConditionName();
    const QString convertedName = QString::fromStdString(conditionName);
    QVariant val;
    rangeSet ranges;
    auto cond = context->Condition(conditionName);
    for ( auto& r : cond.Ranges())
    {
        ranges.emplaceBack(QVariant::fromStdVariant(r));
    }

    StringConditionWrapper::Type convertedType;
    switch(s.Type(context))
    {
    case Cartomancer::Type::INVALID:
        //error out
        break;
    case Cartomancer::Type::Unconditional:
        convertedType = StringConditionWrapper::Type::Unconditional;
        break;
    case Cartomancer::Type::Substitution:
        convertedType = StringConditionWrapper::Type::Substitution;
        val = convertedName;
        break;
    case Cartomancer::Type::Switch:
        convertedType = StringConditionWrapper::Type::Switch;
        break;
    case Cartomancer::Type::Range:
        convertedType = StringConditionWrapper::Type::Range;
        bool rangesAreNumbers = true;
        auto firstRange = ranges.front().toFloat(&rangesAreNumbers);
        if(rangesAreNumbers && firstRange > 0)
        {
            ranges.push_front("0");
        }
        if(rangesAreNumbers)
        {
            ranges.emplace_back(QString::number(ranges.back().toFloat()*1.5));
        }
        break;
    }

    if (convertedType != StringConditionWrapper::Type::Unconditional)
    {
        if(!val.isValid())
        {
            val = ranges[0];
        }
        //I was going to make this a set initially PRECISELY because this is the behaviour I wanted...
        bool isNew = true;
        for(auto& c: conditions)
        {
            if(dynamic_cast<StringConditionWrapper*>(c)->getName() == convertedName)
            {
                //Do some additional checks to make sure we're updating ranges, didn't mess types, etc...
                isNew = false;
            }
        }

        if(isNew)
        {
            conditions.append(new StringConditionWrapper(this, convertedName, convertedType, std::move(ranges), val));
        }
    }
    for( auto child : s.Children())
    {
        registerCondition(child);
    }
}

void DecoratedStringProvider::wrapConditions()
{
    /* some type conversion
     *
     * Using QVariants here is a lot more work that I'm frontloading and
     * bringing to C++ to try and avoid having unexpected behaviour in QML
     *
     * May not even be necessary, but what I want is for things like
     * assigning values and comparing in the QML side to be as smooth and
     * easy as possible
     */

    bool listHasChanged = false;
    for(const auto elem: context->Conditions())
    {
        auto cond = elem.second;
        const QString convertedName = QString::fromStdString(elem.first);
        QVariant val;
        rangeSet ranges;
        for ( auto& r : cond.Ranges())
        {
            ranges.emplaceBack(QVariant::fromStdVariant(r));
        }

        size_t valType = 0;
        if(!ranges.empty())
        {
            valType = cond.Ranges().begin()->index();
        }

        StringConditionWrapper::Type convertedType;
        switch(cond.Type())
        {
        case Cartomancer::Type::INVALID:
            //error out
            break;
        case Cartomancer::Type::Unconditional:
            //We don't need this in the UI
            convertedType = StringConditionWrapper::Type::Unconditional;
            continue;
        case Cartomancer::Type::Substitution:
            convertedType = StringConditionWrapper::Type::Substitution;
            val = convertedName;
            valType = 4;
            break;
        case Cartomancer::Type::Switch:
            convertedType = StringConditionWrapper::Type::Switch;
            break;
        case Cartomancer::Type::Range:
            convertedType = StringConditionWrapper::Type::Range;
            const bool rangesAreInts =  valType == 2;
            bool conversionOk = true;
            auto firstRange = ranges.front().toFloat(&conversionOk);

            if(!conversionOk)
            {
                errorMessage = "Error converting ranges when importing Cartomancer condition -> " + QString::fromStdString(elem.first);
                emit errorMessageChanged();
            }

            if (firstRange > 0)
            {
                ranges.push_front(rangesAreInts ? 0 : 0.0f);
            }

            ranges.emplace_back(rangesAreInts ? static_cast<int>(ranges.back().toFloat()*1.5f) : ranges.back().toFloat()*1.5f);
            break;
        }

        switch (valType)
        {
            //null
            case 0:
                break;
            //bool
            case 1:
                [[fallthrough]];
            //int
            case 2:
                [[fallthrough]];
            //float
            case 3:
                val = ranges.front();
                break;
            //string
            case 4:
                if(ranges.empty())
                {
                    //This is a substitution
                    val = QVariant(convertedName);
                }
                else
                {
                    val = ranges.front();
                }
                break;
        }

        bool isNew = true;
        for(const auto& cond: conditions)
        {
            if(cond->getName() == convertedName)
            {
                isNew = false;
                break;
            }
        }
        if(isNew)
        {
            auto c = new StringConditionWrapper(this, convertedName, convertedType, std::move(ranges), val);

            conditions.append(c);
            QObject::connect(c, &StringConditionWrapper::valueChanged, this, &DecoratedStringProvider::onConditionValueUpdated);
            listHasChanged = true;
        }
    }
    if(listHasChanged)
    {
        std::sort( conditions.begin(), conditions.end()
                 , [](StringConditionWrapper* a, StringConditionWrapper* b)
                    {
                        if(a->getType() == b->getType())
                        {
                            return a->getName() < b->getName();
                        }
                        return a->getType() < b->getType();
                    });
        emit conditionListChanged();
    }
}

QString DecoratedStringProvider::unfold(const Cartomancer::Text& s, size_t colorIdx) const
{
    QString colour{};
    switch(s.Type(context))
    {
    case Cartomancer::Type::INVALID:
        //error out
        break;
    case Cartomancer::Type::Unconditional:
        colour = default_colour;
        break;
    case Cartomancer::Type::Substitution:
        colour = subs_colours[colorIdx%subs_colours.size()];
        break;
    case Cartomancer::Type::Switch:
        colour = switch_colours[colorIdx%switch_colours.size()];
        break;
    case Cartomancer::Type::Range:
        colour = range_colours[colorIdx%range_colours.size()];
        break;
    }

    const QString colourTag{"<font color="+colour+">"};
    auto str = s.Content(context);
    QString text{colourTag+QString::fromStdString(str)};
    size_t child_idx = 0;
    auto replacement_idx = text.indexOf(QChar::SpecialCharacter::ObjectReplacementCharacter);
    while (replacement_idx > 0)
    {
        //QChar::unicode() returns the raw char16_t underneath. Another victim of equating the two things
        child_idx = text[replacement_idx+1].unicode();
        text.replace(replacement_idx, 2, unfold(s.getChild(child_idx),(++colorIdx)));
        replacement_idx = text.indexOf(QChar::SpecialCharacter::ObjectReplacementCharacter);
    }
    return text+"</font>";
}

StringConditionWrapper::StringConditionWrapper(DecoratedStringProvider* parent, QString n, Type t, rangeSet&& r, QVariant v)
    : name{n}
    , conditionType{t}
    , value{v}
{
    rangeList = r;
    setParent(parent);
}

StringConditionWrapper::StringConditionWrapper(const StringConditionWrapper& other)
    : name{other.name}
    , conditionType{other.conditionType}
    , rangeList{other.rangeList}
    , value{other.value}
{
    setParent(other.parent());
}

StringConditionWrapper::StringConditionWrapper(StringConditionWrapper&& other) noexcept
    : name{std::move(other.name)}
    , conditionType{other.conditionType}
    , rangeList{std::move(other.rangeList)}
    , value{std::move(other.value)}
{
    setParent(other.parent());
}

StringConditionWrapper& StringConditionWrapper::operator=(const StringConditionWrapper& other)
{
    name = other.name;
    conditionType = other.conditionType;
    rangeList = other.rangeList;
    value = other.value;
    setParent(other.parent());
    return *this;
}

void StringConditionWrapper::setValue(const QVariant& v) noexcept
{
    value = v;
    auto provider = dynamic_cast<DecoratedStringProvider*>(parent());
    auto& original = provider->Context()->Condition(name.toStdString());
    if( original == Cartomancer::Condition::None())
    {
        //error out
    }
    else
    {
        switch( original.Value().index() )
        {
        //null
        case 0:
            break;
        //bool
        case 1:
            original.setValue(v.toBool());
            break;
        //int
        case 2:
            original.setValue(v.toInt());
            break;
        //float
        case 3:
            original.setValue(v.toFloat());
            break;
        //string
        case 4:
            original.setValue(v.toString().toStdString());
            break;
        }
        emit valueChanged();
    }
}
