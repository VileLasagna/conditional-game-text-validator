/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */


#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFile>
#include <QStringList>
#include <QQmlContext>
#include <QQuickStyle>
#include <QQuickWindow>

#include <iostream>

#include "include/decoratedstringprovider.hpp"


#ifndef PARSER_VERSION_MAJOR
    constexpr const size_t parserVersionMajor = 0;
#else
    constexpr const size_t parserVersionMajor = PARSER_VERSION_MAJOR;
#endif

#ifndef PARSER_VERSION_MINOR
    constexpr const size_t parserVersionMinor = 0;
#else
    constexpr const size_t parserVersionMinor = PARSER_VERSION_MINOR;
#endif

auto main(int argc, char *argv[]) -> int
{
    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle("Fusion");



    QString appVersion(QString::number(parserVersionMajor));
    appVersion.append(".");
    appVersion.append(QString::number(parserVersionMinor));

    app.setApplicationName("Foreteller");
    app.setApplicationVersion(appVersion);

    QQmlApplicationEngine engine;

    QScopedPointer<DecoratedStringProvider> provider(new DecoratedStringProvider);

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);

    qmlRegisterType<StringConditionWrapper>("vilelasagna.foreteller", 1, 0, "StringCondition");
    qmlRegisterSingletonInstance("vilelasagna.foreteller", 1, 0, "DecoratedStringProvider", provider.get());
    engine.rootContext()->setContextProperty("qtversion", QString(qVersion()));
    engine.load(QUrl(QStringLiteral("qrc:/vilelasagna/foreteller/qml/Main.qml")));
    //engine.loadFromModule("vilelasagna.foreteller", "Main");
    QGuiApplication::setWindowIcon(QIcon(":/Cartomancer-logo.png"));

    return app.exec();
}
