# How to use Foreteller

Foreteller is a frontend for the **Cartomancer** library
output.

You can either load your own files, as long as they're plain text, or simply type or
paste you own content straight away.

Use the tabs at the top to navigate to different panes. If you get any errors, they
will be shown up in the bottom of the application

---

## About Cartomancer

**Cartomancer** is a _C++_ library for parsing text with conditional output for games
and other applications. By default it interprets any text between [square brackets]
to be conditional and it tries to infer how these are supposed to work from what
that looks like.

---

### Known Issues

- **Cartomancer** 1.0 does not handle correctly when non-range conditions are declared
with different number of options. In particular:
  - Any condition that has only one declared option will be interpreted as a boolen switch.
  if you later redeclare it with more than two options, the condition will be in a broken state.
  This is targeted for a fix on the next version.
  > i.e: [char.Gender | MALE_TEXT]
  >
  > will be interepreted as an on/off. A later call of:
  >
  > [char.Gender | MALE_TEXT | FEMALE_TEXT | OTHER_TEXT]
  >
  > will have the condition internally
  be misconscructed (it'll have true/false/0/1/2 values, which is an invalid state)
  >
  > To work around this issue, add empty statements where they might otherwhise be missing:
  >
  >[char.Gender | MALE_TEXT||]

---

### Markers and Decoration

**Foreteller** makes use of **Qt**'s text renderer to provide SOME decoration support.
This is as much by accident as it is intentional so, take it or leave it. This can
be done through the use of HTML tags. If you're curious about what's possible or
want to make actual use of it, **Qt** provides a [handy list](https://doc.qt.io/qt-6/richtext-html-subset.html)
of what's supported. **All the text provided is ultimately interpreted as UTF-8**

**Cartomancer** relies on specific markers to split up and read your text. If you're
using the library on your own projects, you can configure those to suit your needs.
**Foreteller** just uses the default markers. These are as follows

- String Delimiters are `[`square brackets`]`
 - Use these to tell them where your string begin and end
- Statement separators are vertical pipe `|`
  - **Cartomancer** will use these to split up the condition variables and each different option
  from each other
- Argument separators are empty space "` `"
  - **Cartomancer** will use this to separate multiple thresholds for range checks

Check the examples to see these in action.


### String types

There are four types of strings in **Cartomancer**:

 - _Unconditional_ strings
 - _Substitution_ variables
 - _Switch_ statements
 - _Range comparison_ statements



#### Unconditional

> This is some unconditional text. Just plain old lorem ipsum

As the name implies, these have no condition attached and are always active. They are
your "common text" that will be shared



#### Substitution

> Hello [user.Name]! How are you feeling in this [timeOfDay]?

These are placeholder to be replaced by string values. These can often be names
and nicknames, as well as variable locations etc...



#### Switch

> I'll have you know, I'm a proud [user.Gender|comrade|coward]!

> If you're happy and you know it! [user.happy|[user.knowsHappy|Clap your hands!]]

Switches will match a variable to predetermined outcomes. This is always a perfect
match comparison. For strings with one or two options, **Cartomancer** will interpret
this these as on/off toggles (so in the second example up top, if the user is not happy, then
the output is an empty string). If there is more than 2 options, then a numeric
value is assigned which control the chosen option. Make sure the same condition
always has the same order


#### Range
>You did a [player.Score 0 2 5|poor job.|good job!|great job!!]

>You can also specify a minimum [likeSo 10 15|This option only shows between 10 and 14.99| And this one for 15 and beyond]

Ranges will read the values provided as floating point numbers and do a check to see
where the value provided falls within a specified range. In the first example above,
"poor job" would be displayed if player.score was in the [0,2) range. The final
range is always a "lower limit" and anything beyond that will give the same result



---



## Input Tab

In this tab you can input the content that you want to parse and check using **Cartomancer**.
Text input here will be processed once you click the `Parse` button. Doing so will
take you to the `Parsing` tab which will have your formatted text and conditions.
We'll talk more about that screen later.

Clicking the `Reset parser` button will reset the state of the **Cartomancer** system
underneath. Your text will need to be re-interpreted to get new conditions and all
values will be reset to default.

The `Clear Input` button instead will clear whatever text is in the text input area.
If you're typing directly here this could lose your work! Be careful as **there is
undo operation here**. To prevent accidental clicks, this button needs to be held
for a short while before it activates.

The `Load File` button will allow you to try and load an external document and paste
its content on the text input area. **Foreteller** only supports plain text files.
Additionally, as mentioned before the text has to be either plain or what **Qt** calls
"RichText" (see "Markers and Decoration" for more info). So even though you, say,
load some markdown text normally, the markdown itself won't get processed. So keep
that in mind. **Cartomancer** itself does no processing of any kind, this is all
on the application side of things. Keep that in mind if you're interested in using
**Cartomancer** on its own.

Finally, you can load a few examples to just get a feel to how the library and
application work. These will load some pre-written texts that you can then parse
and play with.




---




## Parsing Tab

This is where the fun happens. Here you will get the text that has been processed
through **Cartomancer** and be able to check all the different outcomes possible.
On the left-hand side, **Foreteller** will create controls for each variable detected.
The controls will also allow you to set new values for those conditions and you
can see the results in real time.

These conditions and values are stored until you press `Reset Parser` on the Input Tab,
even if you parse different text.

**Foreteller** tries to help you quickly identify the different pieces of the text through
multiple colours. Each individual text item will have its own colour. _Unconditional_
text is white and each of the other condition types have their own individual palette
to pick from (so all substitution text will be various shades of green, for example)

For _Range_ check variables, **Foreteller** will add up to two additional values to
its own controls: First a `0` value if it's missing, and then a value that is `150%`
of the highest listed value. This is just to make it easier for you to see different
results. The value marked in pink is the highest value listed on the actual text.
Also, keep in mind that the helper notches are evenly spaced and not proportional
to the values they hold. For a bit of clarity regarding where you should be in your
options, you can check the colour of the individual indicators




---




## About Tab

Just some information regarding versions and licensing, etc.... With a few links
to relevant places. Check it out if you're looking for the actual **Cartomancer**
code or some information about myself, including how to show your support if
you liked either this tool or library




---




## Help Tab

It is this one.

This is it.

I hope it WAS helpful =)

If you're looking for more in-depth or specific stuff, try heading to the repository
and checking the information there. I'm also happy to answer any questions you might
have about either piece of code (well, at least try to XD)





---




## The Future of Foreteller

I guess an obvious question is:

> Will this be getting more updates/features in the future?

And there is some low hanging fruit, perhaps, like integrating translation support,
perhaps a light and dark mode, supporting **Cartomancer** configuration in **Foreteller**...

But there is also a huge pile of other projects demanding attention so I'm not going
to make any promises. It's a project I really enjoyed so although I don't plan on
sticking with it for now, I also don't rule it out. At the very least I plan on
hosting a working version of **Foreteller** on my own site indefinitely
