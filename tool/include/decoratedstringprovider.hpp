/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

#ifndef DECORATEDSTRINGPROVIDER_HPP
#define DECORATEDSTRINGPROVIDER_HPP

#include <QObject>
#include <QtQml/qqmlregistration.h>
#include <QVariant>
#include <QUrl>

#include <vector>

#include "cartomancer/text.hpp"
#include "cartomancer/condition.hpp"

using rangeSet = QList<QVariant>;

class DecoratedStringProvider;

class StringConditionWrapper : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(QString   name       READ getName                        NOTIFY nameChanged          FINAL)
    Q_PROPERTY(Type      type       READ getType                        NOTIFY typeChanged          FINAL)
    Q_PROPERTY(rangeSet  ranges     READ getRanges                      NOTIFY rangesChanged        FINAL)
    Q_PROPERTY(qsizetype rangeCount READ getRangeCount                  NOTIFY rangeCountChanged    FINAL)
    Q_PROPERTY(QVariant  value      READ getValue       WRITE setValue  NOTIFY valueChanged)


public:

    enum Type { Unconditional,
                Substitution,
                Switch,
                Range};
    Q_ENUM(Type)

    StringConditionWrapper( DecoratedStringProvider* parent = nullptr, QString n = ""
                          , Type t = Type::Unconditional
                          , rangeSet&& r = {}
                          , QVariant v = {});
    StringConditionWrapper(const StringConditionWrapper& other);
    StringConditionWrapper(StringConditionWrapper&& other) noexcept;

    StringConditionWrapper& operator=(const StringConditionWrapper& other);

    [[nodiscard]] const QString&  getName() const noexcept    { return name;}
    [[nodiscard]] Type      getType() const noexcept    { return conditionType;}
    [[nodiscard]] rangeSet  getRanges() const noexcept  { return rangeList;}
    [[nodiscard]] qsizetype getRangeCount() const noexcept {return rangeList.count();}
    [[nodiscard]] QVariant  getValue() const noexcept {return value;}

    void setValue(const QVariant& v) noexcept;

signals:

    void nameChanged();
    void typeChanged();
    void rangesChanged();
    void rangeCountChanged();
    void valueChanged();

private:

    QString name;
    Type conditionType;
    rangeSet rangeList;
    QVariant value;
};

class DecoratedStringProvider : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

    Q_PROPERTY(QString rawContent                           READ RawContent WRITE setRawContent NOTIFY rawContentChanged)
    Q_PROPERTY(QString content                              READ Content                        NOTIFY contentChanged)
    Q_PROPERTY(QList<StringConditionWrapper*> conditionList READ conditionList                  NOTIFY conditionListChanged)
    Q_PROPERTY(QString errorMessage                         READ ErrorMessage                   NOTIFY errorMessageChanged)
    Q_PROPERTY(QString cartomancerVersion                   READ CartomancerVersion             NOTIFY cartomancerVersionChanged)
    Q_PROPERTY(QString foretellerHelpText                   READ HelpText                       NOTIFY helpTextChanged)


public:
    explicit DecoratedStringProvider(QObject *parent = nullptr);

    void setRawContent(const QString& newContent);

    void setContext(std::shared_ptr<Cartomancer::Context> ctxt);

    [[nodiscard]] QString CartomancerVersion() const;

    [[nodiscard]] std::shared_ptr<Cartomancer::Context> Context() const noexcept {return context;}

    [[nodiscard]] QString Content() const;
    [[nodiscard]] const QString& RawContent() const noexcept;
    [[nodiscard]] const QString HelpText() const noexcept;
    [[nodiscard]] const QString& ErrorMessage() const noexcept;
    [[nodiscard]] const QList<StringConditionWrapper*> conditionList() const noexcept {return conditions;}

    Q_INVOKABLE void reset();
    Q_INVOKABLE void init();
    Q_INVOKABLE QString loadFile(const QString& filepath) const;
    Q_INVOKABLE QString loadFile(const QUrl& filepath) const;
    Q_INVOKABLE QString loadExample(size_t exampleIndex);

signals:
    void cartomancerVersionChanged() const;
    void conditionListChanged() const;
    void contentChanged() const;
    void helpTextChanged() const;
    void rawContentChanged() const;
    void errorMessageChanged() const;

private slots:

    void onConditionValueUpdated();

private:

    std::unique_ptr<Cartomancer::Text> processedContent;
    QString rawContent;
    QString richFormatContent;
    QList<StringConditionWrapper*> conditions;

    mutable QString errorMessage;

    void registerCondition(const Cartomancer::Text& s);
    void wrapConditions();

    void setContent(Cartomancer::Text&& s);

    [[nodiscard]]QString unfold(const Cartomancer::Text& s, size_t colorIdx = 0) const;

    std::shared_ptr<Cartomancer::Context> context;

    QString default_colour;
    std::vector<QString> subs_colours;
    std::vector<QString> switch_colours;
    std::vector<QString> range_colours;


};

#endif // DECORATEDSTRINGPROVIDER_HPP
