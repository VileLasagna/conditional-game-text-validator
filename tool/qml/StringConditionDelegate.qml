/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import vilelasagna.foreteller

Rectangle {
    id: conditionDelegate
    width: ListView.view.width - 10
    color: (ListView.view.currentIndex !== index) && bgArea.containsMouse? "#88e80070"  : "transparent"
    anchors.leftMargin: 6
    radius: 5

    property int itemMargin : 3
    property int topHeight : 40
    property int botHeight : 50 + (sliderVisible ? 20 : 0)
    // Switch type with 2 values will be a boolean toggle
    property bool sliderVisible : modelData.type === StringCondition.Range || modelData.rangeCount > 2

    height: topHeight + botHeight + itemMargin

    function select() {
        conditionDelegate.ListView.view.currentIndex = index;
    }

    MouseArea {
        id:bgArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: conditionDelegate.select()
    }


    Rectangle
    {
        id: nameContainer
        color: "#EE313131"
        anchors {
            top:   parent.top
            left:  parent.left
            right: parent.right
        }
        height: conditionDelegate.topHeight

        Text {
            id:nameText
            anchors {
                fill: parent

                topMargin: 3
                bottomMargin: 3
                leftMargin: 5
                rightMargin: 5
            }
            font {
                pointSize: 22
                weight: Font.Bold
            }

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
            fontSizeMode: Text.Fit
            minimumPointSize: 12

            color: "#FFFFFFFF"
            text: modelData.name
         }
    }


    Rectangle {
        id: valueContainer
        color: "#EE111111"
        anchors {
            top:   nameContainer.bottom
            topMargin: conditionDelegate.itemMargin
            left:  parent.left
            right: parent.right
        }
        height: conditionDelegate.botHeight

        TextField {
            id: subsInputField
            anchors {
                fill: parent

                topMargin: 3
                bottomMargin: 3
                leftMargin: 5
                rightMargin: 5
            }

            visible : (modelData.type === StringCondition.Substitution)
            color: "#FFFFFFFF"
            font.pointSize: 16
            placeholderTextColor: "#DD434343"
            placeholderText: modelData.name
            background: Rectangle {
                color: "#FF111111"
                border.color: "goldenrod"
                border.width: 1
            }
            text: modelData.value
            onTextEdited: modelData.value = text
            onPressed: conditionDelegate.select()
        }


        Rectangle {
            color: "transparent"
            anchors.fill: parent
            visible : modelData.type === StringCondition.Switch

            // For Booleans (and other binaries)
            Switch {
                id: conditionToggle
                visible: modelData.rangeCount === 2

                anchors.centerIn: parent
                checked: modelData.value
                checkable: true
                onToggled: modelData.value = checked

                indicator: Rectangle {
                        implicitWidth: 78
                        implicitHeight: 30
                        x: conditionToggle.leftPadding
                        y: parent.height / 2 - height / 2
                        radius: 15
                        color: conditionToggle.checked ? "#FFe80070" : "#EE313131"
                        border.color: conditionToggle.checked ? "goldenrod" : "#88888888"
                        Behavior on color {
                            ColorAnimation { duration: 150 }
                        }

                        Rectangle {
                            x: conditionToggle.checked ? parent.width - width : 0
                            Behavior on x {
                                NumberAnimation { duration: 150 }
                            }

                            width: 30
                            height: 30
                            radius: 15
                            color: conditionToggle.down ? "#FFFFFF" : (conditionToggle.checked ? "#EEEEEE" : "#AAAAAA")
                            border.color: conditionToggle.checked ? (conditionToggle.down ? "gold" : "goldenrod") : "#999999"
                            Behavior on border.color {
                                ColorAnimation { duration: 150 }
                            }
                        }
                    }
            }
            Slider {
                id: switchSlider
                visible: modelData.rangeCount > 2
                anchors {
                    bottom: parent.bottom
                    bottomMargin: 10
                    left: parent.left
                    leftMargin: 20
                    right: parent.right
                    rightMargin: 20
                }
                from: modelData.rangeCount <= 0 ? 0 : modelData.ranges[0]
                to: modelData.rangeCount <= 0 ? 0 : modelData.ranges[modelData.ranges.length - 1]
                stepSize: 1
                snapMode: Slider.SnapAlways
                value: modelData.value
                onMoved: modelData.value = value

                background: Rectangle {
                        x: switchSlider.leftPadding
                        y: switchSlider.topPadding + switchSlider.availableHeight / 2 - height / 2
                        implicitWidth: 200
                        implicitHeight: 4
                        width: switchSlider.availableWidth
                        height: implicitHeight
                        radius: 2
                        color: "#EE313131"

                        Rectangle {
                            width: switchSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#FFe80070"
                            radius: 2
                        }
                    }

                    handle: Rectangle {
                        x: switchSlider.leftPadding + switchSlider.visualPosition * (switchSlider.availableWidth - width)
                        y: switchSlider.topPadding + switchSlider.availableHeight / 2 - height / 2
                        implicitWidth: 26
                        implicitHeight: 26
                        radius: 13
                        color: switchSlider.pressed ? "#f0f0f0" : "#f6f6f6"
                        border.color: "#bdbebf"
                        Text {
                            anchors.centerIn: parent
                            color: "#FF111111"
                            fontSizeMode: Text.Fit
                            font.weight: Font.Bold
                            font.pointSize: 14
                            minimumPointSize: 7
                            text: switchSlider.value.toFixed(0)
                        }
                    }
            }
            SliderNotches {
                slider: switchSlider
            }

        }

        Rectangle {
            color: "transparent"
            anchors.fill: parent
            visible : modelData.type === StringCondition.Range

            Slider {
                id: rangeSlider
                anchors {

                    bottom: parent.bottom
                    bottomMargin: 10
                    left: parent.left
                    leftMargin: 20
                    right: parent.right
                    rightMargin: 20
                }
                from: modelData.rangeCount <= 0 ? 0 : modelData.ranges[0]
                to: modelData.rangeCount <= 0 ? 0 : modelData.ranges[modelData.ranges.length - 1]
                snapMode: Slider.SnapAlways
                value: modelData.value
                live: true
                onPressedChanged: {
                    if (! pressed)
                        modelData.value = value
                }

                background: Rectangle {
                        x: rangeSlider.leftPadding
                        y: rangeSlider.topPadding + rangeSlider.availableHeight / 2 - height / 2
                        implicitWidth: 200
                        implicitHeight: 4
                        width: rangeSlider.availableWidth
                        height: implicitHeight
                        radius: 2
                        color: "#EE313131"

                        Rectangle {
                            width: rangeSlider.visualPosition * parent.width
                            height: parent.height
                            color: "goldenrod"
                            radius: 2
                        }
                    }

                    handle: Rectangle {
                        x: rangeSlider.leftPadding + rangeSlider.visualPosition * (rangeSlider.availableWidth - width)
                        y: rangeSlider.topPadding + rangeSlider.availableHeight / 2 - height / 2
                        implicitWidth: 56
                        implicitHeight: 26
                        radius: 6
                        color: switchSlider.pressed ? "#f0f0f0" : "#f6f6f6"
                        border.color: "#bdbebf"
                        Text {
                            anchors.fill: parent
                            color: "#FF111111"
                            font.weight: Font.Bold
                            fontSizeMode: Text.Fit
                            font.pointSize: 14
                            minimumPointSize: 7
                            text: rangeSlider.value.toFixed(2)
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
            }
            SliderNotches {
                slider: rangeSlider
                tagPenultimate: true
            }
        }
    }
}
