/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import vilelasagna.foreteller

RowLayout {

    Connections {
        target: DecoratedStringProvider

        function onConditionListChanged() {
            conditionList.model = DecoratedStringProvider.conditionList
        }
    }

    id: parserViewRoot

    property int maxTextHeight;

    Rectangle {
        id: leftContainer
        Layout.maximumWidth:  mainWindow.width / 5
        Layout.fillWidth: true
        Layout.fillHeight: true
        color: "transparent"

        ListView {
            id: conditionList
            anchors.fill: parent
            boundsBehavior: Flickable.StopAtBounds
            delegate: StringConditionDelegate{}
            spacing: 15
            model: DecoratedStringProvider.conditionList
            ScrollBar.vertical: ScrollBar { contentItem:Rectangle{implicitWidth: 6; radius: 6; color:"#FFe80070"}}
            highlightFollowsCurrentItem: true
            highlightResizeVelocity: 1000
            highlightMoveVelocity: -1
            highlight: Rectangle {
                color: "#88a88326"
                radius: 5
            }
            clip:true

        }
    }

    Rectangle {
        id: rightContainer
        color: "#BB0f0f0f"
        Layout.fillWidth: true
        Layout.fillHeight: true
        ScrollView {
            id: textDisplayView
            anchors.fill: parent
            contentWidth: availableWidth
            contentHeight: parserViewRoot.maxTextHeight
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            ScrollBar.horizontal.interactive: false
            ScrollBar.vertical.contentItem: Rectangle{implicitWidth: 6; radius: 6; color:"#FFe80070"}
            ScrollBar.vertical.policy: ScrollBar.AsNeeded
            ScrollBar.vertical.interactive: true

            Rectangle {
                // A container of semi-fixed size to prevent the ScrollView from resetting its position
                // every time the text changes (due to variable tweaking)
                height: parserViewRoot.maxTextHeight
                anchors {
                    top:  parent.top
                    left: parent.left
                    right: parent.right
                    rightMargin: 16
                }
                color: "transparent"

                TextArea {
                    id: textDisplayArea
                    readOnly: true
                    anchors {
                        top:   parent.top
                    }
                    width: parent.width

                    textFormat: TextEdit.RichText
                    text: DecoratedStringProvider.content
                    placeholderText: "Once processed, your text will be interpreted and displayed here!!"
                    placeholderTextColor: "#DD636363"
                    font.pointSize: 16
                    wrapMode: TextEdit.WordWrap
                }
            }
        }
    }
}
