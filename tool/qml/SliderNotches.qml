/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick

Rectangle{
    id: notchArea
    visible: slider.visible
    color: "transparent"

    property Item slider;
    property bool tagPenultimate;

    anchors {
        bottom: slider.top
        top: parent.top
        left: slider.left
        right: slider.right
        leftMargin: slider.handle.implicitWidth/2
        rightMargin: slider.handle.implicitWidth/2
    }
    Row {
        id: infoRow
        anchors {
            top: parent.top
            topMargin: 3
            bottom: parent.bottom
            bottomMargin: 5
            left: parent.left
            leftMargin: 0
            right: parent.right
            rightMargin: 0
        }
        spacing: (notchArea.width - modelData.rangeCount)/(Math.max(1,(modelData.rangeCount - 1)))
        Repeater {
            id: notchRepeater
            model: modelData.ranges
            anchors.fill: parent
            delegate: Rectangle {
                clip: false
                width: 1
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                }

                color: "transparent"
                Text {
                    id: valueText
                    anchors{
                        horizontalCenter: parent.left
                        top: parent.top
                    }
                    text: modelData
                    color: slider.value >= modelData? "gold":"#CCCCCCCC"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                Rectangle {
                    width: 1
                    radius: 1
                    color: tagPenultimate && (index === (notchRepeater.count - 2)) ? "magenta" : slider.value >= modelData? "goldenrod":"#CCCCCCCC"
                    anchors {
                        horizontalCenter: valueText.horizontalCenter
                        top: valueText.bottom
                        topMargin: 0
                        bottom: parent.bottom
                        bottomMargin: 0
                    }
                }
            }
        }
    }
}
