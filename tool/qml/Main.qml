/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtQuick.Controls

import vilelasagna.foreteller

ApplicationWindow {
    id: mainWindow
    width: 1280
    height: 720
    visible: true
    title: Qt.application.name
    background: Rectangle {
        anchors.fill: parent
        color : "#FF1f1f1f"
    }

    Connections {
        target: DecoratedStringProvider
        function onContentChanged(text){
            if(DecoratedStringProvider.content !== "")
            {
                headerArea.setCurrentIndex(1);
            }
        }
    }

    SequentialAnimation {
        id: footerFlash

        ColorAnimation {
            target: footerArea
            property: "color"
            from: "transparent"
            to: "#DD666666"
            duration: 100
            easing.type: Easing.OutQuad
        }
        ColorAnimation {
            target: footerArea
            property: "color"
            from: "#DD666666"
            to: "transparent"
            duration: 100
            easing.type: Easing.InQuad
        }
        ColorAnimation {
            target: footerArea
            property: "color"
            from: "transparent"
            to: "#DD666666"
            duration: 100
            easing.type: Easing.OutQuad
        }
        ColorAnimation {
            target: footerArea
            property: "color"
            from: "#DD666666"
            to: "transparent"
            duration: 100
            easing.type: Easing.InQuad
        }
    }

    footer: Rectangle {
        id: footerArea
        height: 20
        color: "transparent"
        Text {
            id: errorText
            text: DecoratedStringProvider.errorMessage
            color: "orangered"
            onTextChanged: {
                if (text !== "")
                {
                    footerFlash.start();
                }
            }

        }
    }

    header: TabBar {
        id: headerArea

        height: 50
        background: Rectangle {
            anchors.fill: parent
            color : "#FF1f1f1f"
        }

        ParserTabButton {
            text: qsTr("Input")
        }
        ParserTabButton {
            text: qsTr("Parsing")
        }
        ParserTabButton {
            text: qsTr("About")
        }
        ParserTabButton {
            text: qsTr("Help")
        }
    }

    SwipeView
    {
        id: mainArea

        anchors.fill:parent
        anchors.topMargin: 5
        anchors.bottomMargin: 3
        currentIndex: headerArea.currentIndex
        orientation: Qt.Vertical
        interactive: false
        clip:true

        Rectangle {
            id: inputArea
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "transparent"

            StringInputView {
                id: inputView
                anchors.fill:parent
                onParseBtnClicked: {
                    DecoratedStringProvider.init();
                    DecoratedStringProvider.rawContent = textContent;
                }
            }
        }

        Rectangle
        {
            id: interpreterArea
            color: "transparent"
            Layout.fillWidth: true
            Layout.fillHeight: true

           StringParserView {
               id: parserView
               anchors.fill: parent
               maxTextHeight: inputView.contentHeight
           }
        } //InterpreterArea

        Rectangle {
            id: aboutArea
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "transparent"
            AboutView {
                id: aboutView
                anchors.fill: parent
            }

        }// aboutArea

        Rectangle {
            id: helpArea
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "transparent"

            Rectangle {
                    id: textContainer
                    color: "#BB0f0f0f"
                    anchors {
                        fill:parent
                        topMargin: 40
                        bottomMargin: 20
                        leftMargin: 50
                        rightMargin: 50
                    }

                    Image {
                        source:"qrc:/Cartomancer-logo.png"
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        opacity: 0.1
                    }
                    ScrollView {
                        id: helpView
                        anchors.fill: parent
                        contentWidth: availableWidth
                        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                        ScrollBar.horizontal.interactive: false
                        ScrollBar.vertical.contentItem: Rectangle{implicitWidth: 6; radius: 6; color:"#FFe80070"}
                        ScrollBar.vertical.policy: ScrollBar.AsNeeded
                        ScrollBar.vertical.interactive: true

                        TextArea {
                            id: helpDisplayArea
                            textFormat: TextEdit.MarkdownText
                            readOnly: true
                            color: "#FFD0D0D0"
                            width: parent.contentWidth
                            text:  DecoratedStringProvider.foretellerHelpText
                            font.pointSize: 16
                            wrapMode: TextEdit.WordWrap
                            onLinkActivated: Qt.openUrlExternally(link)

                        }
                    }
                }
        }
    } //StackLayout
}
