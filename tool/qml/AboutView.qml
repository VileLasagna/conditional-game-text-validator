/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import vilelasagna.foreteller

Rectangle {
    id: aboutInnerContainer
    anchors {
        fill:parent
        topMargin: 40
        bottomMargin: 20
        leftMargin: 50
        rightMargin: 50
    }
    color: "transparent"

    RowLayout {
        anchors.fill: parent
        spacing: 15
        Rectangle {
            id: aboutSoftwareContainer
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: "#BB0f0f0f"
            ScrollView {
                anchors {
                    fill: parent
                }
                contentWidth: availableWidth
                contentHeight: softwareColumn.height

                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.horizontal.interactive: false
                ScrollBar.vertical.contentItem: Rectangle{implicitWidth: 6; radius: 6; color:"#FFe80070"}
                ScrollBar.vertical.policy: ScrollBar.AsNeeded
                ScrollBar.vertical.interactive: true

                ColumnLayout {
                    id: softwareColumn
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        rightMargin: 15
                    }
                    spacing: 5

                    Text {
                        text: "About this software"

                        color: "#FFFFFFFF"
                        Layout.fillWidth: true
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 30
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Rectangle {

                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        height: aboutTitle.height + appVersionText.height
                        color: "transparent"

                        Text {
                            id: aboutTitle
                            text: Qt.application.name

                            color: "#FFFFFFFF"
                            width: parent.width
                            font.weight: Font.Bold
                            fontSizeMode: Text.Fit
                            font.pointSize: 16
                            minimumPointSize: 7
                            horizontalAlignment: Text.AlignHCenter
                            onLinkActivated: Qt.openUrlExternally(link)
                            textFormat: Text.MarkdownText
                            wrapMode: Text.WordWrap
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Text {
                            id: appVersionText
                            text: "version: " + Qt.application.version

                            color: "#FFCCCCCC"
                            width: parent.width
                            fontSizeMode: Text.Fit
                            font.pointSize: 12
                            minimumPointSize: 7
                            horizontalAlignment: Text.AlignHCenter
                            onLinkActivated: Qt.openUrlExternally(link)
                            textFormat: Text.MarkdownText
                            wrapMode: Text.WordWrap
                            anchors.top: aboutTitle.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Rectangle {
                        //spacer
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        height: 5
                        color: "transparent"
                    }

                    Text {
                        text: qsTr("Foreteller is built using the Cartomancer library, version ") + DecoratedStringProvider.cartomancerVersion + qsTr("

and the Qt application toolkit, version " + qtversion)

                        color: "#FFCCCCCC"
                        Layout.fillWidth: true
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 12
                        minimumPointSize: 7
                        verticalAlignment: Text.AlignTop
                        horizontalAlignment: Text.AlignHCenter
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Rectangle {
                        //spacer
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        height: 15
                        color: "transparent"
                    }

                    RowLayout {
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        Layout.preferredHeight: implicitHeight
                        Image {
                            clip: true
                            source:"qrc:/Cartomancer-logo.png"
                            //Layout.fillHeight: true
                            Layout.preferredWidth: aboutSoftwareContainer.width * 0.2
                            fillMode: Image.PreserveAspectFit
                            opacity: 1
                            MouseArea {
                                anchors.fill: parent
                                onClicked: Qt.openUrlExternally("https://gitlab.com/VileLasagna/cartomancer")
                            }
                        }

                        Text {
                            text: qsTr("The Cartomancer library is open source and licensed under a modified version of the [Anti-Capitalist Software License](https://anticapitalist.software). You can find it on [its home in Gitlab](https://gitlab.com/VileLasagna/cartomancer)")

                            color: "#FFCCCCCC"
                            Layout.fillWidth: true
                            font.weight: Font.Bold
                            fontSizeMode: Text.Fit
                            font.pointSize: 12
                            minimumPointSize: 7
                            horizontalAlignment: Text.AlignLeft
                            onLinkActivated: Qt.openUrlExternally(link)
                            textFormat: Text.MarkdownText
                            wrapMode: Text.WordWrap
                        }
                    } //RowLayout
                    Rectangle {
                        //spacer
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        height: 35
                        color: "transparent"
                    }

                    Text {
                        text: qsTr("This application is licensed under the [GNU General Public License (GPL), version3](https://www.gnu.org/licenses/gpl-3.0.html)
                                    You can find it [the same repository](https://gitlab.com/VileLasagna/cartomancer/-/tree/main/tool)
                                    under the \"tool\" directory")
                        color: "#FFCCCCCC"
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 12
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Rectangle {
                        //spacer
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        height: 50
                        color: "transparent"
                    }

                    Text {
                        text: qsTr("Qt for WebAssembly is licensed under the [GNU General Public License (GPL), version3](https://doc.qt.io/qt-6/gpl.html)")

                        color: "#FFCCCCCC"
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 12
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Rectangle {
                        //spacer
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        height: 25
                        color: "transparent"
                    }

                    Text {
                        text: qsTr("For more information, visit [Qt.io](https://qt.io). You can find the source code either there or get a copy from [here](https://vilelasagna.ddns.net/public-srcs/qt-everywhere-src-"+qtversion+".tar.xz)")

                        color: "#FFCCCCCC"
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignTop | Qt.AlignCenter
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 12
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }
                } //ColumnLayout
            } //ScrollView
        } //aboutSoftwareContainer

        Rectangle {
            id: aboutDeveloperContainer
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: "#BB0f0f0f"

            ScrollView {
                anchors {
                    fill: parent
                }
                contentWidth: availableWidth
                contentHeight: aboutMeColumn.height

                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.horizontal.interactive: false
                ScrollBar.vertical.contentItem: Rectangle{implicitWidth: 6; radius: 6; color:"#FFe80070"}
                ScrollBar.vertical.policy: ScrollBar.AsNeeded
                ScrollBar.vertical.interactive: true

                ColumnLayout {
                    id: aboutMeColumn
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        rightMargin: 15
                    }
                    spacing: 5
                    Text {
                        text: "About me"

                        color: "#FFFFFFFF"
                        Layout.fillWidth: true
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 30
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }


                    Image {
                        clip: true
                        source:"qrc:/PPGLasagna.jpg"
                        Layout.fillHeight: true
                        Layout.preferredWidth: aboutDeveloperContainer.width * 0.6
                        fillMode: Image.PreserveAspectFit
                        opacity: 1
                        Layout.alignment: Qt.AlignCenter
                    }

                    Rectangle {
                        //spacer
                        Layout.fillWidth: true
                        height: 5
                        color: "transparent"
                    }

                    Text {
                        text: "Hey there. I'm Vile Lasagna, an Aotearoa Zealand based software dev. Make sure to check out my other work at [my blog](https://vilelasagna.ddns.net)
(probably where you are right now, tbh) and in my [public Gitlab repos](https://gitlab.com/VileLasagna). If you want to connect, you can find me in [Mastodon](https://mastodon.gamedev.place/@VileLasagna)."

                        color: "#FFFFFFFF"
                        Layout.fillWidth: true
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 14
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Text {
                        text: "I have also put together some other tools as well and if you're into coding, maybe there's a thing or two I've written that might interest you"

                        color: "#FFFFFFFF"
                        Layout.fillWidth: true
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 14
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Text {
                        text: "If you're having a good time using Cartomancer or Foreteller and have some loose change, feel free to send it my way, it'll be very appreciated"

                        color: "#FFFFFFFF"
                        Layout.fillWidth: true
                        font.weight: Font.Bold
                        fontSizeMode: Text.Fit
                        font.pointSize: 14
                        minimumPointSize: 7
                        horizontalAlignment: Text.AlignHCenter
                        onLinkActivated: Qt.openUrlExternally(link)
                        textFormat: Text.MarkdownText
                        wrapMode: Text.WordWrap
                    }

                    Image {
                        clip: true
                        source:"qrc:/kofi_bg_tag_white.png"
                        Layout.fillHeight: true
                        Layout.preferredWidth: aboutDeveloperContainer.width * 0.5
                        fillMode: Image.PreserveAspectFit
                        opacity: 1
                        Layout.alignment: Qt.AlignCenter
                        MouseArea {
                            anchors.fill: parent
                            onClicked: Qt.openUrlExternally("https://ko-fi.com/vilelasagna")
                        }
                    }
                } //ColumnLayout
            } //ScrollView
        } //aboutDeveloperContainer
    } //RowLayout
}
