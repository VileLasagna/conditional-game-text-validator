/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Dialogs
import Qt.labs.folderlistmodel

import QtCore

import vilelasagna.foreteller

RowLayout {

    id: inputViewRoot
    signal parseBtnClicked;
    readonly property string textContent: textDisplayArea.text
    readonly property int contentHeight: textDisplayArea.height

    SequentialAnimation {
        id: textDisplayFlash

        ColorAnimation {
            target: textContainer
            property: "color"
            from: "#BB0f0f0f"
            to: "#DD333333"
            duration: 100
            easing.type: Easing.OutQuad
        }
        ColorAnimation {
            target: textContainer
            property: "color"
            from: "#DD333333"
            to: "#BB0f0f0f"
            duration: 100
            easing.type: Easing.InQuad
        }
    }

    Rectangle {
        id: textContainer
        color: "#BB0f0f0f"
        Layout.fillWidth: true
        Layout.fillHeight: true
        ScrollView {
            id: textDisplayView
            anchors.fill: parent
            contentWidth: availableWidth
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            ScrollBar.horizontal.interactive: false
            ScrollBar.vertical.contentItem: Rectangle{implicitWidth: 6; radius: 6; color:"#FFe80070"}
            ScrollBar.vertical.policy: ScrollBar.AsNeeded
            ScrollBar.vertical.interactive: true

            TextArea {
                id: textDisplayArea
                textFormat: TextEdit.PlainText
                color: "#FFD0D0D0"
                width: parent.contentWidth
                placeholderText: "Write or load your decorated text!"
                placeholderTextColor: "#DD636363"
                font.pointSize: 16
                wrapMode: TextEdit.WordWrap
            }
        }
    }

    Rectangle {
        id: controlsContainer
        Layout.maximumWidth:  mainWindow.width / 5
        Layout.fillWidth: true
        Layout.fillHeight: true
        color: "transparent"

        ColumnLayout {
            anchors {
                top: parent.top
                left:parent.left
                right: parent.right
                bottom: parent.bottom
                topMargin: 25
                bottomMargin: 15
                leftMargin: 5
                rightMargin: 5
            }

            InputControlButton {
                id: parseBtn
                text: qsTr("Parse")
                ToolTip.text: qsTr("Parse your text through Cartomancer")

                onClicked: inputViewRoot.parseBtnClicked();
            }

            InputControlButton {
                id: resetBtn
                text: qsTr("Reset parser")
                ToolTip.text: qsTr("Clear your current processed text and conditions")

                onClicked: {
                    DecoratedStringProvider.reset();
                }
            }

            Rectangle {
                Layout.fillWidth: true
                //Layout.fillHeight: true
                Layout.minimumHeight: 5

                color: "transparent"
            }

            InputControlButton {
                id: clearBtn
                text: qsTr("Clear input")
                ToolTip.text: qsTr("Clear your raw text. Hold to activate")
                safeguard: true

                onSafePressed: {
                    textDisplayFlash.start();
                    textDisplayArea.text = "";
                }
            }

            Rectangle {
                Layout.fillWidth: true
                //Layout.fillHeight: true
                Layout.minimumHeight: 5

                color: "transparent"
            }

            InputControlButton {
                id: loadFileBtn
                text: qsTr("Load File")
                ToolTip.text: qsTr("Load a plain text file from your system")
                onClicked: filepicker.open()

            }

            Rectangle {
                id: spacerB
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.minimumHeight: 50
                color: "transparent"
            }

            InputControlButton {
                id: exampleOneBtn
                text: qsTr("Example 1")
                ToolTip.text: qsTr("Load a built-in example with basic functionality")

                onClicked: {
                    textDisplayFlash.start();
                    textDisplayArea.text = DecoratedStringProvider.loadExample(0);
                }
            }
        } //ColumnLayout
    } //controlsContainer

    FileDialog {
        id: filepicker
        currentFolder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        defaultSuffix: ".txt"
        fileMode: FileDialog.OpenFile
        onAccepted: {
            textDisplayFlash.start();
            textDisplayArea.text = DecoratedStringProvider.loadFile(selectedFile);
        }
    }
}
