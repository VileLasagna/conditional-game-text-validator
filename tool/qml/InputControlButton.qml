/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Button {
    id: control
    Layout.fillWidth: true
    height: 50

    ToolTip.delay: 1000
    ToolTip.timeout: 5000
    ToolTip.visible: hovered

    property bool safeguard: false
    signal safePressed;

    NumberAnimation {
        id: fillTimer
        target: safeFill
        property: "width"
        from: 0
        to: control.width
        duration: 750

        onFinished: {
            safeFill.width = 0;
            if(control.safeguard)
            {
                control.safePressed();
            }
        }
    }

    contentItem: Text {
        text: control.text
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        color: control.hovered ? "White" : "#99FFFFFF"

        font {
            pointSize: 22
            weight: Font.Bold
        }
        fontSizeMode: Text.Fit
        minimumPointSize: 12
    }

    background: Rectangle {
        color: control.pressed? "black" : control.hovered ? "#EE313131" : "#99111111"
        opacity: enabled ? 1 : 0.3
        border.color: control.pressed ? "gold" : control.hovered ?  "goldenrod" : "#99434343"
        border.width: 2
        radius: 5
        Rectangle {
            id: safeFill
            anchors {
                top: parent.top
                left: parent.left
                bottom: parent.bottom
            }
            radius: 5
            color: "#88e80070"
        }
    }

    onPressed: {
        if(control.safeguard)
        {
            fillTimer.restart();
        }
    }
    onReleased: {
        if(control.safeguard)
        {
            fillTimer.stop();
            safeFill.width = 0;
        }
    }
}
