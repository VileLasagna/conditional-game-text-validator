/** Foreteller - an example appication for Cartomancer
 * Copyright (C) 2023  Jehferson W. C. R. Mello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://gitlab.com/VileLasagna/cartomancer/-/blob/main/tool/LICENSE.MD>.
 */

import QtQuick
import QtQuick.Controls

TabButton {
    id: control
    property bool isCurrent: TabBar.tabBar.currentIndex === TabBar.index
    contentItem: Text {
            text: control.text
            opacity: enabled ? 1.0 : 0.3
            color: control.down ? "#FFFFFFFF" : (control.isCurrent ? "goldenrod" : "#DD434343")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight

            font {
                pointSize: 22
                weight: Font.Bold
            }
            fontSizeMode: Text.Fit
            minimumPointSize: 12
        }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 50
        color: control.hovered ? "#99111111":"transparent"
        opacity: enabled ? 1 : 0.3
        border.color: control.isCurrent ? "#99434343" : "#99111111"
        border.width: 2
        radius: 5
    }
}
